
package jose_alexandre.mapadesala;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Curso {

    @SerializedName("idcurso")
    @Expose
    private Integer idcurso;
    @SerializedName("dsccurso")
    @Expose
    private String dsccurso;

    public Integer getIdcurso() {
        return idcurso;
    }

    public void setIdcurso(Integer idcurso) {
        this.idcurso = idcurso;
    }

    public String getDsccurso() {
        return dsccurso;
    }

    public void setDsccurso(String dsccurso) {
        this.dsccurso = dsccurso;
    }

    @Override
    public String toString() {
        return dsccurso;
    }
}
