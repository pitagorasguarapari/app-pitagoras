package jose_alexandre.mapadesala;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class BuscaCurso extends AppCompatActivity {

    List<Curso> cursosDisponiveis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busca_curso);

        final ListView lista = (ListView) findViewById(R.id._dynamicListaCursos);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://pitagorasguarapari-net.umbler.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PitagorasService api = retrofit.create(PitagorasService.class);

        Call<Cursos> call = api.getCursos();
        call.enqueue(new Callback<Cursos>() {
            @Override
            public void onResponse(Call<Cursos> call, Response<Cursos> response) {
                //int statusCode = response.code()
                if(response.isSuccessful()){
                    Cursos cursos = response.body();
                    cursosDisponiveis = cursos.getCursos();
                    ArrayAdapter<Curso> adapter = new ArrayAdapter<Curso>(BuscaCurso.this, android.R.layout.simple_list_item_1,cursosDisponiveis);
                    lista.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<Cursos> call, Throwable t) {

            }
        });


        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                createAlertDialog(cursosDisponiveis.get(position));
                //Toast.makeText(BuscaCurso.this, "Testando lista", Toast.LENGTH_SHORT).show();

            }
        });

        /*List<String> cursos = new ArrayList<>();
        cursos.add("C Computacao");
        cursos.add("Arquitetura");
        cursos.add("Pedagogia");
        cursos.add("Engenharia");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,cursos);
        lista.setAdapter(adapter);*/
    }

    public void createAlertDialog(final Curso curso) {
        AlertDialog.Builder alertbox = new AlertDialog.Builder(this);

        alertbox.setTitle("Escolha seu período")
                .setItems(R.array.alert, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int pos) {
                        Intent i = new Intent(BuscaCurso.this,GradeActivity.class);
                        i.putExtra("periodo",pos+1);
                        i.putExtra("curso",curso.getIdcurso());
                        startActivity(i);
//pos will give the selected item position

                    }
                });
        alertbox.show();
    }
}