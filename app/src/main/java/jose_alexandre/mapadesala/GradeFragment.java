package jose_alexandre.mapadesala;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


public class GradeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    // TODO: Rename and change types of parameters
    private ItemGrade itemGrade;


    public GradeFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static GradeFragment newInstance(ItemGrade itemGrade) {
        GradeFragment fragment = new GradeFragment();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, itemGrade);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemGrade = (ItemGrade) getArguments().getSerializable(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_grade, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(itemGrade.getDsccurso());
        ((AppCompatActivity)getActivity()).getSupportActionBar().setSubtitle(itemGrade.getDscperiodo());
        TextView textViewDisciplina = getActivity().findViewById(R.id.textViewDisciplina);
        textViewDisciplina.setText(itemGrade.getDscdisciplina());

        TextView textViewProfessor = getActivity().findViewById(R.id.textViewProfessor);
        textViewProfessor.setText(itemGrade.getDscprofessor());

        TextView textViewSala= getActivity().findViewById(R.id.textViewSala);
        textViewSala.setText(itemGrade.getDscsala());

        TextView textViewHorario = getActivity().findViewById(R.id.textViewHorario);
        textViewHorario.setText(itemGrade.getDtinicio()+" - "+itemGrade.getDtfim());

        Button buttonComoChegar = getActivity().findViewById(R.id.buttonComoChegar);
        buttonComoChegar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(),"Em breve...",Toast.LENGTH_LONG).show();
            }
        });

    }


}
