package jose_alexandre.mapadesala;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GradeActivity extends AppCompatActivity {

    //TextView conteudoTextView;



    //ProgressBar gradeProgressBar;

    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grade);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        int periodo = getIntent().getIntExtra("periodo",0);
        int curso = getIntent().getIntExtra("curso",0);
        //conteudoTextView = findViewById(R.id.conteudoGradeTextView);
        //gradeProgressBar = findViewById(R.id.gradeProgressBar);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://pitagorasguarapari-net.umbler.net/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        PitagorasService api = retrofit.create(PitagorasService.class);

        Call<Grade> call = api.getGrade(curso,periodo);
        call.enqueue(new Callback<Grade>() {
            @Override
            public void onResponse(Call<Grade> call, Response<Grade> response) {
                if (response.isSuccessful()) {
        //            gradeProgressBar.setVisibility(View.INVISIBLE);
                    Grade grade = response.body();
                    setupViewPager(grade);
          //          conteudoTextView.setText(grade.toString());

                }
            }

            @Override
            public void onFailure(Call<Grade> call, Throwable t) {

            }
        });
    }

    private void setupViewPager(Grade grade) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(GradeFragment.newInstance(grade.getGrade().get(0)), "SEGUNDA");
        adapter.addFragment(GradeFragment.newInstance(grade.getGrade().get(1)), "TERÇA");
        adapter.addFragment(GradeFragment.newInstance(grade.getGrade().get(2)), "QUARTA");
        adapter.addFragment(GradeFragment.newInstance(grade.getGrade().get(3)), "QUINTA");
        adapter.addFragment(GradeFragment.newInstance(grade.getGrade().get(4)), "SEXTA");
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home){
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}

