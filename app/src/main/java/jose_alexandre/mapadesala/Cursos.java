
package jose_alexandre.mapadesala;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Cursos {

    @SerializedName("cursos")
    @Expose
    private List<Curso> cursos = null;

    public List<Curso> getCursos() {
        return cursos;
    }

    public void setCursos(List<Curso> cursos) {
        this.cursos = cursos;
    }

}
