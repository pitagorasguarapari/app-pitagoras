package jose_alexandre.mapadesala;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    private Button buttonAluno;
    private Button buttonConhecer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonAluno = (Button) findViewById(R.id.buttonAluno);
        buttonConhecer = (Button) findViewById(R.id.buttonConhecer);

        buttonAluno.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(MainActivity.this,BuscaCurso.class);
                startActivity(i);

            }
        });

        buttonConhecer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.faculdadepitagoras.com.br/"));
                startActivity(i);
            }
        });
    }
}
