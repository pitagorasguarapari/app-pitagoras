
package jose_alexandre.mapadesala;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class ItemGrade implements Serializable {

    @SerializedName("turma")
    @Expose
    private String turma;
    @SerializedName("dsccurso")
    @Expose
    private String dsccurso;
    @SerializedName("dscperiodo")
    @Expose
    private String dscperiodo;
    @SerializedName("dia")
    @Expose
    private String dia;
    @SerializedName("dscdisciplina")
    @Expose
    private String dscdisciplina;
    @SerializedName("dscprofessor")
    @Expose
    private String dscprofessor;
    @SerializedName("dscsala")
    @Expose
    private String dscsala;
    @SerializedName("dtinicio")
    @Expose
    private String dtinicio;
    @SerializedName("dtfim")
    @Expose
    private String dtfim;

    public String getTurma() {
        return turma;
    }

    public void setTurma(String turma) {
        this.turma = turma;
    }

    public String getDsccurso() {
        return dsccurso;
    }

    public void setDsccurso(String dsccurso) {
        this.dsccurso = dsccurso;
    }

    public String getDscperiodo() {
        return dscperiodo;
    }

    public void setDscperiodo(String dscperiodo) {
        this.dscperiodo = dscperiodo;
    }

    public String getDia() {
        return dia;
    }

    public void setDia(String dia) {
        this.dia = dia;
    }

    public String getDscdisciplina() {
        return dscdisciplina;
    }

    public void setDscdisciplina(String dscdisciplina) {
        this.dscdisciplina = dscdisciplina;
    }

    public String getDscprofessor() {
        return dscprofessor;
    }

    public void setDscprofessor(String dscprofessor) {
        this.dscprofessor = dscprofessor;
    }

    public String getDscsala() {
        return dscsala;
    }

    public void setDscsala(String dscsala) {
        this.dscsala = dscsala;
    }

    public String getDtinicio() {
        return dtinicio;
    }

    public void setDtinicio(String dtinicio) {
        this.dtinicio = dtinicio;
    }

    public String getDtfim() {
        return dtfim;
    }

    public void setDtfim(String dtfim) {
        this.dtfim = dtfim;
    }

    @Override
    public String toString() {
        return dscsala+getDscprofessor();
    }
}
