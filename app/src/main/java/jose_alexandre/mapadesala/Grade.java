
package jose_alexandre.mapadesala;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Grade {

    @SerializedName("grade")
    @Expose
    private List<ItemGrade> grade = null;

    public List<ItemGrade> getGrade() {
        return grade;
    }

    public void setGrade(List<ItemGrade> grade) {
        this.grade = grade;
    }

    @Override
    public String toString() {
        String conteudo = "";
        for (ItemGrade item : grade) {
            conteudo +=item.getDia()+" "+item.getDscdisciplina()+" "+item.getDscprofessor()+" "+item.getDscsala()+"\n\n";
        }
        return conteudo;
    }
}
