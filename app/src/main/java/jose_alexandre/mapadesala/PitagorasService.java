package jose_alexandre.mapadesala;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface PitagorasService {
    @GET("cursos")
    Call<Cursos> getCursos();
    @GET("grade/{curso}/{periodo}")
    Call<Grade> getGrade(@Path("curso") int idCurso, @Path("periodo") int numeroPeriodo);
}
